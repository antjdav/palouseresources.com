<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

return array(

  '*' => array(
		'tablePrefix' => 'craft',
	),

  'craft.dev' => array(
    'server' => 'localhost',
    'user' => 'root',
		'password' => 'root',
		'database' => 'palouse-dev'
	),

	'nxnw.io' => array(
    'server' => 'localhost',
		'user' => 'palouseresources',
		'password' => 'Zt6bttqcLfq8BA',
		'database' => 'palouseresources_com'
	),

	// '.com' => array(
  //   'server' => 'localhost',
	// 	'user' => 'palouse',
	// 	'password' => '51$O3!125eXh',
	// 	'database' => 'palouse_com'
	// )

);

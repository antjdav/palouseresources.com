<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(

  '*' => array(
    'omitScriptNameInUrls' => true,
    'autoLoginAfterAccountActivation' => true,
    'purgePendingUsersDuration' => 'P1M',
    'maxInvalidLogins' => 5,
    'maxSlugIncrement' => 400
  ),

  // Setup for local development.
  'craft.dev' => array(
    'devMode' => true,
    'environmentVariables' => array(
      'siteUrl' => 'http://palouse.craft.dev/'
    )
  ),

  // Setup for remote stage.
  'nxnw.io' => array(
    'devMode' => true,
    'environmentVariables' => array(
      'siteUrl' => 'http://palouseresources.nxnw.io/', // when doing this put '{siteUrl}' in for > Site URL
    )
  )

);

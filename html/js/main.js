$(document).ready(function(){ 
    $('#characterLeft').text('1000 characters left');
    $('#message').keydown(function () {
        var max = 1000;
        var len = $(this).val().length;
        if (len >= max) {
            $('#characterLeft').text('You have reached the limit');
            $('#characterLeft').addClass('red');
            $('#btnSubmit').addClass('disabled');            
        } 
        else {
            var ch = max - len;
            $('#characterLeft').text(ch + ' characters left');
            $('#btnSubmit').removeClass('disabled');
            $('#characterLeft').removeClass('red');            
        }
    });    
});

  function slider() {

        if ($(window).width() > 770) {

            if (one == 1) {
                // $(".slide1").attr("class", "slide2");
                //
                $(".slide1").stop().animate({
                    "opacity": "0"
                }, 800);

                $(".coldpress").stop().animate({
                    "opacity": "0"
                }, 600, function() {

                    $(".coldpress").html("<p class=\"coldpress-bar\"></p><p class=\"coldpress-p\">TRY SOMETHING FRESH</p>");
                    $(".coldpress").stop().animate({
                        "opacity": "1"
                    }, 600);

                });


                $(".anchor").stop().animate({
                    "opacity": "0"
                }, 800, function() {

                    $(".anchor").html("<p class=\"anchor-p\">Enjoy it with us here, or on the go.</p>");
                    $(".anchor").stop().animate({
                        "opacity": "1"
                    }, 800);

                });



                $("#btnId").stop().animate({
                    "opacity": "0"
                }, 350, function() {

                    $("#btnId").html("VISIT US");
                    $("#btnId").stop().animate({
                        "opacity": "1"
                    }, 350);

                });



                one = 0;
                two = 1;

            } else if (two == 1) {

                $(".slide1").stop().animate({
                    "opacity": "1"
                }, 800);

                $(".coldpress").stop().animate({
                    "opacity": "0"
                }, 600, function() {

                    $(".coldpress").html("<p class=\"coldpress-bar\"></p><p class=\"coldpress-p\">TOLA'S FAVORITE THING</p>");
                    $(".coldpress").stop().animate({
                        "opacity": "1"
                    }, 600);

                });

                $(".anchor").stop().animate({
                    "opacity": "0"
                }, 800, function() {

                    $(".anchor").html("<p class=\"anchor-p\">could also be your favorite thing <br>(he loves the thai peanut sauce).</p>");
                    $(".anchor").stop().animate({
                        "opacity": "1"
                    }, 800);
                });



                $("#btnId").stop().animate({
                    "opacity": "0"
                }, 350, function() {

                    $("#btnId").html("MORE INFO");
                    $("#btnId").stop().animate({
                        "opacity": "1"
                    }, 350);

                });


                two = 0;
                one = 1;


            }


        };

    };


